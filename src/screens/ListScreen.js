import React from 'react';
import { Text, View, StyleSheet, FlatList } from 'react-native';

// this is a component
const ListScreen = function() {

  //keys on React lists:
  //keep track of the data
  //avoid the rebuilt of the entire list from scratch
  //must be a string
  const friends = [
    { name: 'Friend #1', age: '20' },
    { name: 'Friend #2', age: '45' },
    { name: 'Friend #3', age: '32' },
    { name: 'Friend #4', age: '27' },
    { name: 'Friend #5', age: '53' },
    { name: 'Friend #6', age: '30' },
    { name: 'Friend #7', age: '20' },
    { name: 'Friend #8', age: '20' },
    { name: 'Friend #9', age: '20' }
  ];

  return (
    <FlatList
      keyExtractor = { friend => friend.name }
      data = { friends }
      renderItem = { function({ item }){
        return <Text style = { styles.textStyle }>
          { item.name } - Age { item.age }</Text>;
      }}
    />
  );
}

const styles = StyleSheet.create({
  textStyle: {
    marginVertical: 50,
    marginHorizontal: 10
  }
});

export default ListScreen;
