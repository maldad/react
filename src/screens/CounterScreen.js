import React, {useReducer} from 'react';
import {View, Text, StyleSheet, Button} from 'react-native';

// action -> how to change my state
const reducer = (state, action) => {
  // state === {counter: number}
  // action === { type: 'increment' || 'decrement', payload: 1 }
  switch (action.type) {
    case 'increase_counter':
      // return state.counter + action.payload;
      // this doesn't work because you have to return and OBJECT
      return {...state, counter: state.counter + action.payload};
    case 'decrease_counter':
      return {...state, counter: state.counter - action.payload};
    default:
      return state;
  }
};

// functional component
// this component is a JS function!
const CounterScreen = () => {
  // const [counter, setCounter] = useState(10);
  // dispatch -> call my reducer
  const [state, dispatch] = useReducer(reducer, {counter: 10});

  return (
    <View>
      <Button
        title="Increase"
        onPress={() => {
          // don't do this!
          // counter++;
          dispatch({type: 'increase_counter', payload: 1});
        }}
      />
      <Button
        title="Decrease"
        onPress={() => {
          dispatch({type: 'decrease_counter', payload: 1});
        }}
      />
      <Text>
        Current count: {state.counter}
      </Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default CounterScreen;
